package com.anvarco;

import javax.swing.JOptionPane;
import java.io.IOException;

public class Helper {

    private static Custumer c;

    public static int setup(){
        int x = 0;
        boolean valid;
        Helper.c = new Custumer();

        do {
            try{
                x = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese Cantidad de usuarios a crear"));
                if (x == 0){
                    JOptionPane.showMessageDialog(null, "Gracias por usar CityBank", "Gracias...", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }
                valid = true;
            }catch (Exception e){
                System.err.println("Error inicializando el vector de usuarior. Error:  " + e.getMessage() + "\n Causa: \n" + e.getCause());
                JOptionPane.showMessageDialog(null, "Favor ingrese una cantidad Valida", "Error de ingreso", JOptionPane.ERROR_MESSAGE);
                valid = false;
            }
        }while (!valid);


        return (Integer) x;

    }

    public static void run(){
        Helper.menu();
    }

    public static Custumer menu(){

        boolean login;
        do{
            int account = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese su ID"));
            int password = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese su clave"));
            try{
                login = Helper.c.loginAccount(account, password);
                Helper.c = Accounts.getUser(account);
            }catch (Exception e){
                System.err.println("Error al loguearse el usuario " + c.getUsername() +  "  " + e.getMessage());
                JOptionPane.showMessageDialog(null, "Los datos de Acceso son Errados", "Error de ingreso", JOptionPane.ERROR_MESSAGE);
                Helper.c = null;
                login = false;
            }
        }while (!login);

        return Helper.c;
    }

    public static String userMenu(){
        Object selection = JOptionPane.showInputDialog(
                null,
                "Su saldo actual es de:  " + Helper.c.getBalance() + "\n Que tipo de movimiento desea realizar? \n",
                "Mi cuenta...",
                JOptionPane.QUESTION_MESSAGE,
                null,  // null para icono defecto
                new Object[] { "withdraw", "deposit", "transfer", "cancel" },
                "withdraw");

        return (String) selection;
    }

}
